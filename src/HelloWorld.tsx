export interface HelloWorldProps extends React.HTMLAttributes<HTMLHeadingElement> {}

export default function HelloWorld(props: HelloWorldProps) {
  return <h1 {...props}>Hello World!</h1>;
}
