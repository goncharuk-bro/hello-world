import React from 'react';
import { ComponentStory, Meta } from '@storybook/react';

import HelloWorldComponent, { HelloWorldProps } from './HelloWorld';

export default {
  title: 'Example',
  component: HelloWorldComponent,
  argTypes: {},
} as Meta<HelloWorldProps>;

export const HelloWorld: ComponentStory<typeof HelloWorldComponent> = (args) => {
  return <HelloWorldComponent />;
};

HelloWorld.args = {};
