# @goncharuk-bro/hello-world

## Installation

```bash
npm i @goncharuk-bro/hello-world
```

## License

MIT © [Nikolay Goncharuk](https://github.com/GoncharukBro)
